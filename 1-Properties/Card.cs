﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public string Seed
        {
            get { return this.seed; }
        }

        public string Name
        {
            get { return this.name; }
        }

        public int Ordinal
        {
            get { return this.Ordinal; }
        }

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        public override string ToString()
        {
            // TODO understand string interpolation
            return $"{this.GetType().Name}(Name={this.name}, Seed={this.seed}, Ordinal={this.ordial})";
        }

        public override bool Equals(object obj)
        {
            return (obj != null && obj is Card && 
                ((Card)obj).Name.Equals(this.name) && 
                ((Card)obj).Seed.Equals(this.seed) && 
                ((Card)obj).Ordinal == this.ordial);
        }

        public override int GetHashCode()
        {
            // TODO improve
            return base.GetHashCode();
        }
    }

}