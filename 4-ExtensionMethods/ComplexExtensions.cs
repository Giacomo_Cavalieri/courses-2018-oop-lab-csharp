﻿using System;

namespace ExtensionMethods {

    public static class ComplexExtensions
    {
        public static IComplex Add(this IComplex c1, IComplex c2)
        {
            throw new NotImplementedException();
        }

        public static IComplex Subtract(this IComplex c1, IComplex c2)
        {
            throw new NotImplementedException();
        }

        public static IComplex Multiply(this IComplex c1, IComplex c2)
        {
            throw new NotImplementedException();
        }

        public static IComplex Divide(this IComplex c1, IComplex c2)
        {
            throw new NotImplementedException();
        }

        public static IComplex Conjugate(this IComplex c1)
        {
            throw new NotImplementedException();
        }

        public static IComplex Reciprocal(this IComplex c1)
        {
            throw new NotImplementedException();
        }
    }

}