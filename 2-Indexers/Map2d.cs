﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        private readonly IDictionary<Tuple<TKey1, TKey2>, TValue> map = new Dictionary<Tuple<TKey1, TKey2>, TValue>();

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            throw new NotImplementedException();
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get { return this.map[Tuple.Create(key1, key2)]; }
            set { this.map[Tuple.Create(key1, key2)] = value; }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            return map.Where(entry => entry.Key.Item1.Equals(key1))
                      .Select(entry => Tuple.Create(entry.Key.Item2, entry.Value))
                      .ToList();
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            return map.Where(entry => entry.Key.Item2.Equals(key2))
                      .Select(entry => Tuple.Create(entry.Key.Item1, entry.Value))
                      .ToList();
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            return map.Select(entry => Tuple.Create(entry.Key.Item1, entry.Key.Item2, entry.Value))
                      .ToList();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            foreach (TKey1 key1 in keys1)
            {
                foreach (TKey2 key2 in keys2)
                {
                    map.Add(Tuple.Create(key1, key2), generator(key1, key2));
                }
            }
        }

        public int NumberOfElements
        {
            get
            {
                return map.Keys.Count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
